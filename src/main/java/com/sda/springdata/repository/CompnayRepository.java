package com.sda.springdata.repository;

import com.sda.springdata.domain.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CompnayRepository extends CrudRepository<Company, Long> {

    @Query("FROM Company cmp WHERE LOWER (cmp.taxCode)=LOWER(:taxCode) ")
    public Company findByTaxCode(@Param("taxCode") String taxCode);

}
