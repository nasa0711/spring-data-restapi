package com.sda.springdata.controller;

import com.sda.springdata.Exception.MyException;
import com.sda.springdata.domain.Company;
import com.sda.springdata.domain.CompanyStatus;
import com.sda.springdata.repository.CompnayRepository;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.PostUpdate;
import java.util.Optional;

@RestController
@RequestMapping(value = "/company")
public class CompanyController {

    private final CompnayRepository compnayRepository;

    @Autowired
    public CompanyController(CompnayRepository compnayRepository) {
        this.compnayRepository = compnayRepository;
    }

    @DeleteMapping(value = "{id}/delete")
    public ResponseEntity delete(@PathVariable Long id) {
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            compnayRepository.deleteById(id);
            return new ResponseEntity(httpStatus);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }


    @PostMapping(value = "/add")
    public Company addCompany(@RequestBody Company company) {
        return compnayRepository.save(company);
    }

    @PutMapping(value = "{id}/update")
    public ResponseEntity update(@PathVariable Long id, @RequestBody Company company) throws Exception {
        if (compnayRepository.findById(id).isPresent()) {
            Company company1 = compnayRepository.findById(id).get();
            company1.setAddress(company.getAddress());
            company1.setName(company.getName());
            company1.setTaxCode(company.getTaxCode());
            Company updatedCompany = compnayRepository.save(company1);
            return new ResponseEntity(updatedCompany, HttpStatus.OK);
        } else
            throw new Exception();
    }

    @PatchMapping(value = "{cid}/status")
    public ResponseEntity updateStatus(@PathVariable Long id, @RequestBody Company company) throws Exception {
        if (compnayRepository.findById(id).isPresent()) {
            Company company1 = compnayRepository.findById(id).get();
            company1.setCompanyStatus(company.getCompanyStatus());
            Company updatedCompany = compnayRepository.save(company1);
            return new ResponseEntity(updatedCompany, HttpStatus.OK);
        } else
            throw new Exception();
    }

    @GetMapping(value = "/")
    public String helloCompany() {
        return "Hello Senthil";
    }

    /*@GetMapping(value = "/{id}")
    public Company findbyId(@PathVariable Long id) throws MyException {
        if (compnayRepository.findById(id).isPresent()) {
            return compnayRepository.findById(id).get();
        } else {
            throw new MyException("my beautiful exception");
        }
    }*/

    @GetMapping(value = "/{taxCode}")
    public Company findbyTaxcode(@PathVariable String taxC) {
        return compnayRepository.findByTaxCode(taxC);
    }

}




